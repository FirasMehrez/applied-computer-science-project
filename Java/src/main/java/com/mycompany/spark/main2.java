/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;
import java.util.Comparator;

/**
 *
 * @author SAMAR
 */

public class main2 {
	static float sumprice = 0;
	static float avg = 0;
	static float i = 0;
	static float sumprice2 = 0;
	static float priceM2 = 0;
	static float surface = 0;

	public static void main(String[] args) throws IOException {
		List<Property> properties = readPropertyFromCSV("Property-Prices-in-Tunisia.csv");

		System.out.println("***************** display how many properties inside of the dataset *****************");
		System.out.println("there are: " + properties.stream().count() + " properties in this file");

		System.out.println(
				"**********************display the properties categories and cities of the region that contains \"Ariana\" in its name***********************");

		properties.stream().filter(p -> p.getRegion().matches("Ariana"))
				.forEach(e -> System.out.println("category: " + e.getCategory() + " city: " + e.getCity()));

		System.out.println(
				"********************* display the Houses and villas that the price of rent is less than 600 Dt ************************");

		properties.stream()
				.filter(p -> p.getPrice() < 600 && p.getType().equals("For rent")
						&& p.getCategory().equals("Houses and Villas"))
				.forEach(e -> System.out.println("Size of house: " + e.getSize() + "  " + " Region: " + e.getRegion()
						+ " Price: " + e.getPrice()));

		System.out.println(
				"******************* display the luxurious apartments for sale, sorted by price**************************");

		properties.stream().filter(
				p -> p.getPrice() > 500000 && p.getType().equals("For sale") && p.getCategory().equals("Apartments"))
				.sorted(Comparator.comparing(Property::getPrice)).forEach(e -> System.out.println("Category: "
						+ e.getCategory() + "  " + " Region: " + e.getRegion() + " Price: " + e.getPrice()));

		System.out.println(
				"****************** count, for each category, the number of properties for sale and for rent ***************************");

		Map<Object, Map<Object, Long>> nbProp = properties.stream().collect(Collectors.groupingBy(Property::getCategory,
				Collectors.groupingBy(Property::getType, Collectors.counting())));
		System.out.println(nbProp);

		System.out.println(
				"******************** count, for each number of room, how many properties in each region*************************");

		Map<Object, Map<Object, Long>> nbProp_Room = properties.stream().collect(Collectors.groupingBy(
				Property::getRoom_count, Collectors.groupingBy(Property::getRegion, Collectors.counting())));
		System.out.println(nbProp_Room);

		System.out.println(
				"******************* count the cumulative price for each category of properties per region **************************");

		Map<Object, Map<Object, Double>> sumPr = properties.stream()
				.collect(Collectors.groupingBy(Property::getCategory,
						Collectors.groupingBy(Property::getRegion, Collectors.summingDouble(Property::getPrice))));
		System.out.println(sumPr);

		System.out.println(
				"********************* display the apartments that have the highest price ************************");

		System.out.println(properties.stream().filter(p -> p.getCategory().equals("Apartments"))
				.max(Comparator.comparing(Property::getPrice)));

		System.out.println(
				"******************** display the properties in \"Ennasr\" that have the highest price according to the number of rooms and bathrooms *************************");

		System.out.println(properties.stream().filter(p -> p.getRegion().equals("Ennasr"))
				.max(Comparator.comparing(Property::getBathroom_count).thenComparing(Property::getRoom_count)
						.thenComparing(Property::getPrice)));

		System.out.println(
				"******************** count the average of prices of apartments in the Region of Sidi Bou Said *************************");

		properties.stream().filter(p -> p.getCategory().equals("Apartments") && p.getRegion().equals("Sidi Bou Said")
				&& p.getType().equals("For sale") && p.getRoom_count() == 2).forEach(p -> {
					sumprice += p.getPrice();
					i++;
				});
		avg = sumprice / i;
		System.out.println("The average of prices of apartments in the Region of Sidi Bou Said is " + avg);

		System.out.println(
				"********************* count the price of meter2 in the Region of Raoued ************************");

		properties.stream().filter(p -> p.getCategory().equals("Houses and Villas") && p.getRegion().equals("Raoued"))
				.forEach(p -> {
					sumprice2 += p.getPrice();
					surface += p.getSize();
				});
		priceM2 = sumprice2 / surface;
		System.out.println("Price of meter2 in the Region of Raoued is " + priceM2);

	}

	private static List<Property> readPropertyFromCSV(String fileName) {
		List<Property> properties = new ArrayList<>();
		Path pathToFile = Paths.get(fileName);
		// create an instance of BufferedReader
		// using try with resource, Java 7 feature to close resources
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {
			// read the first line from the text file
			String line = br.readLine();
			// loop until all lines are read
			while (line != null) {
				// use string.split to load a string array with the values from // each line of
				// // the file, using a comma as the delimiter
				String[] attributes = line.split(",");
				Property property = createProperty(attributes);
				// System.out.println(wine.toString());
				// adding book into ArrayList
				properties.add(property);
				// read next line before looping // if end of file reached, line would be null
				line = br.readLine();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return properties;
	}

	public static Property createProperty(String[] metadata) {
		String category = metadata[0];
		double room_count = Double.parseDouble(metadata[1]);
		double bathroom_count = Double.parseDouble(metadata[2]);
		double size = Double.parseDouble(metadata[3]);
		String type = metadata[4];
		double price = Double.parseDouble(metadata[5]);
		String city = metadata[6];
		String region = metadata[7];
		double log_price = Double.parseDouble(metadata[8]);
		return new Property(category, room_count, bathroom_count, size, type, price, city, region, log_price);
	}
}
