#!/usr/bin/env python
# coding: utf-8

# In[507]:


import findspark


# In[508]:


import pyspark


# In[509]:


from pyspark.sql import SparkSession


# In[510]:


findspark.init()


## Create spark Session

# In[511]:


spark = SparkSession.builder.appName('propPriceTunisia').getOrCreate()


## Read data from CSV

# In[512]:


data = spark.read.csv('Property-Prices-in-Tunisia.csv',inferSchema=True,header=True)


## Show data 

# In[513]:


data.show()


# In[514]:


from pyspark.ml.feature import StringIndexer


## Create an index for category called categoryIndex for the algorithm 

# In[515]:


indexer = StringIndexer(inputCol="category", outputCol="categoryIndex")
indexed = indexer.fit(data).transform(data)
indexed.show()


## Create an index for city called cityIndex for the algorithm 

# In[516]:


indexer = StringIndexer(inputCol="city", outputCol="cityIndex")
indexed = indexer.fit(indexed).transform(indexed)
indexed.show()


## Create an index for region called regionIndex for the algorithm 

# In[517]:


indexer = StringIndexer(inputCol="region", outputCol="regionIndex")
indexed = indexer.fit(indexed).transform(indexed)
indexed.show()


## Create an index for type called typeIndex for the algorithm 

# In[518]:


indexer = StringIndexer(inputCol="type", outputCol="typeIndex")
indexed = indexer.fit(indexed).transform(indexed)


## Setup log_price as label for the algoritm

# In[519]:


indexed=indexed.withColumn("label",indexed["log_price"])


# In[520]:


indexed.show()


## Delete columns Category Type City Region 

# In[521]:


indexed = indexed.drop("category","type","city","region")


# In[522]:


indexed.show()


# In[523]:


from pyspark.sql.functions import *


##  Filter the dataframe in order to remove -1 values which means that there is no information

# In[524]:


indexed = indexed.filter((indexed.room_count != -1) & (indexed.bathroom_count != -1) & (indexed.size != -1))


# In[525]:


indexed.show()


# In[526]:


from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler


## create vector assembler  in order to put all the columns in one vector

# In[527]:


assembler = VectorAssembler(inputCols = indexed.columns,
                           outputCol='features')


# In[528]:


output_vector = assembler.transform(indexed)


# In[529]:


output_vector.show()


## Split the vector into two vectors one for training data the other one for test data ( 70 % , 30 % )

# In[530]:


(trainingData, testData) = output_vector.randomSplit([0.7, 0.3],seed = 1)


# In[531]:


from pyspark.ml import Pipeline
from pyspark.ml.regression import RandomForestRegressor
from pyspark.ml.feature import VectorIndexer
from pyspark.ml.evaluation import RegressionEvaluator


# ### Init Random Forest Regressor with maxBins 523

# In[532]:


rf = RandomForestRegressor(featuresCol="features",maxBins=523)


## Train data for the model 

# In[533]:


model = rf.fit(trainingData)


## Create the predictions

# In[534]:


predictions = model.transform(testData)


# In[535]:


predictions.select("prediction", "features").show(5)


## Create regression evaluator to get rmse

# In[537]:


evaluator = RegressionEvaluator(
    labelCol="label", predictionCol="prediction", metricName="rmse")
rmse = evaluator.evaluate(predictions)


# In[538]:


print("Root Mean Squared Error (RMSE) on test data = %g" % rmse)

