#!/usr/bin/env python
# coding: utf-8

# In[4]:


import findspark


# In[5]:


findspark.init()


# In[6]:


from pyspark.sql import SparkSession
from pyspark.sql.types import DoubleType
import pyspark.sql.functions as Functions
from pyspark.sql.window import Window


# In[7]:


spark = SparkSession     .builder     .appName("Spark project")     .getOrCreate()


# In[21]:


PropertyData = spark.read.csv('udemy_courses.csv', header=True)


# In[22]:


print(PropertyData.count())


# In[28]:


##// Count all Courses  which are paid


# In[24]:


PropertyData.filter(PropertyData.is_paid == "True").show()


# ## List Courses which subject IS : "Musical Instruments" LIMIT TO 5

# In[27]:


PropertyData.filter((PropertyData.subject == "Musical Instruments")).limit(5).show(truncate=False)


# In[29]:


##List Courses which price  is lower then 100 Dollars  limit to 10


# In[30]:


PropertyData.filter((PropertyData.price < 100)).limit(10).show(truncate=False)


# In[31]:


##List Courses which subject IS : "Business Finance" AND Their price beetween 50 & 100 and show their URLS only


# In[46]:


PropertyData.filter((PropertyData.subject == "Business Finance") & (PropertyData.price>50) & (PropertyData.price<100)).select(PropertyData.url).show(truncate=False)


# In[37]:


##        // List Courses which subject IS : Graphic Design AND  Group by Level and then count



# In[50]:


PropertyData.filter(PropertyData.subject == "Graphic Design").groupBy('level').count().show()


# In[ ]:


##List Courses which subject IS : Graphic Design OR Web Development  AND Sort them by number of subscribers


# In[51]:


PropertyData.filter((PropertyData.subject == "Graphic Design") | (PropertyData.subject == "Web Development") ).orderBy(Functions.desc('num_subscribers')).show(truncate=False)


# In[52]:


##Get course in subject Graphic design which is the most expensive


# In[67]:


PropertyData.filter((PropertyData.subject == "Graphic Design")).select(PropertyData.price,PropertyData.course_title).rdd.max()[1]


# In[ ]:





# In[ ]:




