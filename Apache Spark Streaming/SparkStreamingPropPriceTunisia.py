#!/usr/bin/env python
# coding: utf-8

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType

spark = SparkSession.builder.appName("PropertyPricesTunisiaapp").getOrCreate()

userSchema = StructType().add("category", "string").add("room_count", "float").add("bathroom_count", "float").add("size", "float").\
add("type", "string").add("price", "float").add("city", "string").add("region", "string").add("log_price", "float")

dfCSV = spark.readStream.option("sep", ",").option("header", "true").schema(userSchema).csv("c:/tmp/hive")

dfCSV.createOrReplaceTempView("properties")

number_of_all_properties = spark.sql("select count(*) as number_of_all_properties from properties")
query = number_of_all_properties.writeStream.outputMode("complete").format("console").start()

result2 = spark.sql("select `category`,`city` from properties where `city` LIKE '%Ariana%'")
query = result2.writeStream.outputMode("append").format("console").start()

result3 = spark.sql("select `size`,`region`,`price`,`category`,`type` from properties where (`price` < 600 and `type` = 'For rent' and `category` = 'Apartments')")
query = result3.writeStream.outputMode("append").format("console").start()

result4 = dfCSV.groupBy("category","region").max("price")
query = result4.writeStream.outputMode("complete").format("console").start()

result5 = spark.sql("select `category`,`region`,`type`,count(*) as count from properties group by `category`,`region`,`type`")
query = result5.writeStream.outputMode("complete").format("console").start()

result6 = spark.sql("select `room_count`,`category`,count(*) as number_propertie_per_category from properties group by `category`,`room_count`")
query = result6.writeStream.outputMode("complete").format("console").start()

result7 = spark.sql("select `region`,`category`,sum(`price`) as cumulative_price from properties group by `category`,`region`")
query = result7.writeStream.outputMode("complete").format("console").start()

result8 = spark.sql("select `room_count`,`bathroom_count`, max(`price`) from properties where `region` = 'Ennasr' group by `room_count`,`bathroom_count`")
query = result8.writeStream.outputMode("complete").format("console").start()

result9 = spark.sql("select `region`,`category`,`type`,`room_count`, avg(`price`) as average_price from properties where (`category` = 'Apartments' and `region` = 'Hammamet Nord' and `type` = 'For sale' and `room_count` = 2) group by `region`,`category`,`type`,`room_count`")
query = result9.writeStream.outputMode("complete").format("console").start()

result10 = spark.sql("select `region`,`category`,`type`, sum(`price`)/sum(`size`) as priceM2 from properties where (`category` = 'Houses and Villas' and `region` = 'Raoued') group by `region`,`category`,`type`")
query = result10.writeStream.outputMode("complete").format("console").start()

query.awaitTermination()
