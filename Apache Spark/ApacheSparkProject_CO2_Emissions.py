
# coding: utf-8

# ## <b>Hello Spark </b>

# In[1]:


import findspark
findspark.init()


# In[2]:


import pyspark

from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()

df = spark.sql("select 'spark' as hello ")

df.show()


# ## <b>Project Apache Spark</b>

# In[73]:


from pyspark import SQLContext, SparkContext
sc = SparkContext.getOrCreate()
sql_Context = SQLContext(sc) 


# In[74]:


myDF = spark.read.csv('CO2_Emissions_Canada.csv', header=True)


# ## Total number of cars
# 

# In[75]:


#myDF.show()


# In[76]:


myDF.count()


# ## Filter cars by the engine size

# In[77]:


myDF.filter(myDF.engine_size < 1.5).select(myDF.make,myDF.model,myDF.engine_size).show()


# ## Filter cars by the fuel type

# In[78]:


myDF.filter(myDF.fuel_type == "Z").limit(5).select(myDF.make,myDF.model,myDF.fuel_type).show()


# ## Cars which has the most Co2Emission by Make

# In[79]:


myDF.filter(myDF.make == "BMW").orderBy(myDF.co2_emission).limit(8).select(myDF.make,myDF.model,myDF.fuel_type,myDF.co2_emission).show()


# ## Get cars model via make,vehicle class, and fuel type

# In[80]:


myDF.filter((myDF.make == "CHEVROLET") & (myDF.vehicle_class == "FULL-SIZE") & ((myDF.fuel_type == "Z") | (myDF.fuel_type == "X")
|(myDF.fuel_type == "D"))).limit(10).select(myDF.make,myDF.model,myDF.vehicle_class,myDF.fuel_type).show()


# ## Get cars Model which their Fuel_consumption_City is under 7 per 100 KM

# In[81]:


myDF.filter(myDF.fuel_con_city < 7).orderBy(myDF.fuel_con_city).limit(20).select(myDF.make,myDF.model,myDF.fuel_con_city,myDF.co2_emission).show()


# ## Get cars Model which their Fuel_consumption_highway is under 7 per 100 KM

# In[82]:


myDF.filter(myDF.fuel_con_hwy < 7).orderBy(myDF.fuel_con_hwy).limit(15).select(myDF.make,myDF.model,myDF.fuel_con_hwy,myDF.co2_emission).show()


# ## Get cars Model which their Engine_size is under 3 

# In[83]:


myDF.filter(myDF.engine_size < 3).orderBy(myDF.engine_size).limit(20).select(myDF.make,myDF.model,myDF.engine_size).show()


# ## Get cars Model which their numbers of Cylinders is 4

# In[84]:


myDF.filter(myDF.cylinders == 4).orderBy(myDF.cylinders).limit(20).select(myDF.make,myDF.model,myDF.cylinders).show()  


# ## Display Cars Co2Emission filtering by engineSize and grouping by Make

# In[85]:


myDF.filter((myDF.engine_size > 1) & (myDF.engine_size < 3)).groupBy('make').count().show()

