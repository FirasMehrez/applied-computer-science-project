/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Dell pc
 */
public class Main3 {

    public static void main(String[] args) throws IOException {
        List<Course> courses = readCoursesFromCSV("udemy_courses.csv");
        System.out.println("Number = " + courses.stream().collect(Collectors.toList()).get(0));

        //////////////////////////////////////// SIMPLE REQUESTS //////////////////////////////////////////////////
               
            // Get First element Of the database for verification
        System.out.println("First element :" + courses.stream().collect(Collectors.toList()).get(0));
                
        // Count all Courses  which are paid
        System.out.println("Number of courses paid = " + courses.stream().filter(e -> e.isIs_paid() == true).count());
        // List Courses which subject IS : "Musical Instruments" LIMIT TO 5
        System.out.println("List Courses which subject IS : \"Musical Instruments\" limit to 5 which are of number :" + courses.stream().filter(e -> e.getSubject().equals("Musical Instruments")).count() + " courses");
        courses.stream().filter(e -> e.getSubject().equals("Musical Instruments")).limit(5).collect(Collectors.toList()).forEach(e -> System.out.println(e));

        // List Courses which price  is lower then 100 Dollars  limit to 10
        System.out.println("List Courses which price  is lower then 100 Dollars  limit to 10  which are of number :" + courses.stream().filter(e -> e.getPrice() < 100).count() + " courses");
        courses.stream().filter(e -> e.getPrice() < 100).limit(10).collect(Collectors.toList()).forEach(e -> System.out.println(e));

        //////////////////////////////////////// COMPLEX REQUESTS //////////////////////////////////////////////////
        // List Courses which subject IS : "Business Finance" AND Their price beetween 50 & 100 and show their URLS only
        System.out.println("List Courses which subject IS :Business Finance AND Their price beetween 50 & 100 and show their URLS only ");
        courses.stream().filter(e -> (e.getPrice() > 50 && e.getPrice() < 100) && e.getSubject().equals("Business Finance")).collect(Collectors.toList()).forEach(e -> System.out.println(e.getUrl()));

        // List Courses which subject IS : Graphic Design AND  Group by Level and then count
        System.out.println("List Courses which subject IS : Graphic Design AND  Group by Level and then count ");
        System.out.println(courses.stream().filter(e -> (e.getPrice() > 50 && e.getPrice() < 100) && e.getSubject().equals("Graphic Design")).collect(Collectors.groupingBy(Course::getLevel, Collectors.counting())).toString());

        // List Courses which subject IS : Graphic Design OR Web Development  AND Sort them by number of subscribers
        System.out.println("List Courses which subject IS : Graphic Design OR Web Development  AND Sort them by number of subscribers");
       List<Course> courses1 = courses.stream().filter(e -> e.getSubject().equals("Web Development") || e.getSubject().equals("Graphic Design")).sorted(Comparator.comparing(Course::getNum_subscribers)).limit(5).collect(Collectors.toList());
       Collections.reverse(courses1);
       courses1.forEach(e -> System.out.println(e));

        // Get course in subject Graphic design which is the most expensive
        System.out.println("Get course in subject Graphic design which is the most expensive");

        System.out.println(courses.stream().filter(p -> p.getSubject().equals("Graphic Design"))
                .max(Comparator.comparing(Course::getPrice)).get().getCourse_title());
    }

    private static List<Course> readCoursesFromCSV(String fileName) {
        List<Course> courses = new ArrayList<>();
        Path pathToFile = Paths.get(fileName);
        // create an instance of BufferedReader
        //using try with resource, Java 7 feature to close resources
        try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {
            // read the first line from the text file
            String line = br.readLine();
            // loop until all lines are read
            while (line != null) {
                // use string.split to load a string array with the values from // each line of // the file, using a comma as the delimiter
                String[] attributes = line.split(",");
                Course course = createCourse(attributes);
                //System.out.println(wine.toString());
                // adding book into ArrayList
                courses.add(course);
                // read next line before looping // if end of file reached, line would be null
                line = br.readLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return courses;
    }

    public static Course createCourse(String[] metadata) {
        String course_id = metadata[0];
        String course_title = metadata[1];
        String url = metadata[2];
        boolean is_paid = Boolean.parseBoolean(metadata[3]);
        int price = Integer.parseInt(metadata[4]);
        int num_subscribers = Integer.parseInt(metadata[5]);
        int num_reviews = Integer.parseInt(metadata[6]);
        int num_lectures = Integer.parseInt(metadata[7]);
        String level = metadata[8];
        double content_duration = Double.parseDouble(metadata[9]);

        String subject = metadata[10];
        return new Course(course_id, course_title, url, is_paid, price, num_subscribers, num_reviews, num_lectures, level, content_duration, subject);

    }

}
