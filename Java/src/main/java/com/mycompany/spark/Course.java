/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spark;

/**
 *
 * @author Dell pc
 */
public class Course {

    private String  course_id;
    private String course_title;
    private String url;
    private boolean is_paid;
    private int price;
    private int num_subscribers;
    private int num_reviews;
    private int num_lectures;
    private String level;
    private double content_duration;
    private String subject;

    public Course() {
    }

    public Course(String course_id, String course_title, String url, boolean is_paid, int price, int num_subscribers, int num_reviews, int num_lectures, String level, double content_duration, String subject) {
        this.course_id = course_id;
        this.course_title = course_title;
        this.url = url;
        this.is_paid = is_paid;
        this.price = price;
        this.num_subscribers = num_subscribers;
        this.num_reviews = num_reviews;
        this.num_lectures = num_lectures;
        this.level = level;
        this.content_duration = content_duration;
        this.subject = subject;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isIs_paid() {
        return is_paid;
    }

    public void setIs_paid(boolean is_paid) {
        this.is_paid = is_paid;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNum_subscribers() {
        return num_subscribers;
    }

    public void setNum_subscribers(int num_subscribers) {
        this.num_subscribers = num_subscribers;
    }

    public int getNum_reviews() {
        return num_reviews;
    }

    public void setNum_reviews(int num_reviews) {
        this.num_reviews = num_reviews;
    }

    public int getNum_lectures() {
        return num_lectures;
    }

    public void setNum_lectures(int num_lectures) {
        this.num_lectures = num_lectures;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public double getContent_duration() {
        return content_duration;
    }

    public void setContent_duration(double content_duration) {
        this.content_duration = content_duration;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String toString() {
        return "Course{" + "course_id=" + course_id + ", course_title=" + course_title + ", url=" + url + ", is_paid=" + is_paid + ", price=" + price + ", num_subscribers=" + num_subscribers + ", num_reviews=" + num_reviews + ", num_lectures=" + num_lectures + ", level=" + level + ", content_duration=" + content_duration + ", subject=" + subject + '}';
    }

   
 

}
