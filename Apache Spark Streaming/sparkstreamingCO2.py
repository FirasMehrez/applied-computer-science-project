#!/usr/bin/env python
# coding: utf-8

from pyspark.sql import SparkSession
from pyspark.sql.types import StructType

spark = SparkSession.builder.appName("Co2Emission").getOrCreate()

userSchema = StructType().add("make", "string").add("model", "string").add("vehicle_class", "string").add("engine_size", "float").add("cylinders", "integer").\
add("transmission", "string").add("fuel_type", "string").add("fuel_con_city", "float").add("fuel_con_hwy", "float").add("fuel_con_comb", "float").\
add("fuel_con_comb_mpg", "float").add("co2_emission", "integer")

dfCSV = spark.readStream.option("sep", ",").option("header", "true").schema(userSchema).csv("c:/tmp/hive")
dfCSV.createOrReplaceTempView("Co2Emission")

result = spark.sql("select count(*) from Co2Emission")
result = spark.sql("select make,model,engine_size from Co2Emission where engine_size < 1.5 limit(10)")

query = result.writeStream.outputMode("complete").format("console").start()
result = spark.sql("select make,model,fuel_type from Co2Emission where `fuel_type` = 'Z' limit(10) ")

query = result.writeStream.outputMode("append").format("console").start()

result = spark.sql("select make,model,fuel_type,co2_emission from Co2Emission where `make` = 'BMW' order by co2_emission limit(10) ")

result = spark.sql("select `fuel_con_city` ,count (*) as count Co2Emission where `fuel_con_city` < 7 group by 'fuel_con_city' order by `count` limit(10)")

result = spark.sql("select `fuel_con_hwy` ,count (*) as count Co2Emission where `fuel_con_hwy` < 7 group by 'fuel_con_hwy' order by `count` limit(10)")

result = spark.sql("select `cylinders` ,count (*) as count Co2Emission where `cylinders` = 4 group by 'cylinders' order by `count` limit(10)")

result = spark.sql("select count(*) as count from Co2Emission where `engine_size` between 50 and 100 'Graphic Design' group by `level`")

query = result.writeStream.outputMode("complete").format("console").start()

query = result.writeStream.outputMode("complete").format("console").start()
query.awaitTermination()