package com.mycompany.spark;

public class Property {
 
	@Override
	public String toString() {
		return "Property [category=" + category + ", room_count=" + room_count + ", bathroom_count=" + bathroom_count
				+ ", size=" + size + ", type=" + type + ", price=" + price + ", city=" + city + ", region=" + region
				+ ", log_price=" + log_price + "]";
	}


	private String category;
	private double room_count;
	private double bathroom_count;
	private double size;
	private String type;
	private double price;
	private String city;
	private String region;
	private double log_price;

	public Property() {
		super();
	}


	

	public Property(String category, double room_count, double bathroom_count, double size, String type, double price,
			String city, String region, double log_price) {
		super();
		this.category = category;
		this.room_count = room_count;
		this.bathroom_count = bathroom_count;
		this.size = size;
		this.type = type;
		this.price = price;
		this.city = city;
		this.region = region;
		this.log_price = log_price;
	}




	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public double getRoom_count() {
		return room_count;
	}


	public void setRoom_count(double room_count) {
		this.room_count = room_count;
	}


	public double getBathroom_count() {
		return bathroom_count;
	}


	public void setBathroom_count(double bathroom_count) {
		this.bathroom_count = bathroom_count;
	}


	public double getSize() {
		return size;
	}


	public void setSize(double size) {
		this.size = size;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getRegion() {
		return region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public double getLog_price() {
		return log_price;
	}


	public void setLog_price(double log_price) {
		this.log_price = log_price;
	}
	
	
	
}
