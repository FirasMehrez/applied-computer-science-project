
# coding: utf-8

# In[9]:


import findspark
findspark.init()


# In[10]:


import pyspark

from pyspark.sql import SparkSession

spark = SparkSession.builder.getOrCreate()

df = spark.sql("select 'spark' as hello ")

df.show()


# In[11]:


from pyspark import SQLContext, SparkContext
sc = SparkContext.getOrCreate()
sql_Context = SQLContext(sc) 


# In[24]:


import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
import numpy as np
from sklearn.svm import SVR
from sklearn.model_selection import train_test_split
from pyspark.ml.classification import LinearSVCModel
get_ipython().magic('matplotlib inline')


# In[31]:


df = pd.read_csv('CO2_Emissions_Canada.csv');


# In[32]:


df.head().T


# ### Select some features for the SVM Model

# In[33]:


df_features = df[['EngineSize','Cylinders','FuelConsumptionCity','FuelConsumptionHwy','FuelConsumptionComb','CO2Emissions']]
X = df_features.iloc[:,:-1]
y = df_features.iloc[:,-1]


# In[34]:


X.head(5)


# In[35]:


y.head(5)


# ### Split data into train and test data 

# In[36]:


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


# In[37]:


X_test.head()


# In[38]:


y_test.head()


# ### SVM Regressor

# In[39]:


svr = SVR(C=1.0, epsilon=0.2).fit(X_train,y_train)


# In[40]:


svr.score(X_test,y_test)


# In[41]:


svr.predict(np.array([5.3,8,14.6,10.3,12.7]).reshape(1,-1))


# In[42]:


print('Explained Variance score: %.2f' % svr.score(X_train, y_train))


# ### I try a second model to get a better model accuracy

# ### I select the same features for multiple regression.

# In[43]:


cdf = df[['EngineSize','Cylinders','FuelConsumptionCity','FuelConsumptionHwy','FuelConsumptionComb','CO2Emissions']]
cdf.head(9).T


# ### plotting CO2_Emission values with Engine size:

# In[44]:


plt.scatter(cdf.EngineSize, cdf.CO2Emissions,  color='blue')
plt.xlabel("Engine size")
plt.ylabel("Emissions")
plt.show()


# ### Creating train and test dataset¶
# 

# In[45]:


msk = np.random.rand(len(df)) < 0.8
train = cdf[msk]
test = cdf[~msk]


# ### Train data distribution¶
# 

# In[46]:


plt.scatter(train.EngineSize, train.CO2Emissions,  color='blue')
plt.xlabel("Engine size")
plt.ylabel("Emission")
plt.show()


# ### Multiple Regression Model
# 

# In[47]:


from sklearn import linear_model
regr = linear_model.LinearRegression()
x = np.asanyarray(train[['EngineSize','Cylinders','FuelConsumptionComb']])
y = np.asanyarray(train[['CO2Emissions']])
regr.fit (x, y)
# The coefficients
print ('Coefficients: ', regr.coef_)


# ### Prediction

# In[48]:


from sklearn.metrics import r2_score

y_hat= regr.predict(test[['EngineSize','Cylinders','FuelConsumptionComb']])
x = np.asanyarray(test[['EngineSize','Cylinders','FuelConsumptionComb']])
y = np.asanyarray(test[['CO2Emissions']])
print("Residual sum of squares (MSE): %.2f"
      % np.mean((y_hat - y) ** 2))
print("R2-score: %.2f" % r2_score(y_hat , y) )

# Explained variance score: 1 is perfect prediction
print('Explained Variance score: %.2f' % regr.score(x, y))

