#!/usr/bin/env python
# coding: utf-8

# In[177]:


from pyspark.sql import SparkSession
from pyspark.sql.types import DoubleType
import pyspark.sql.functions as Functions
from pyspark.sql.window import Window
from pyspark.sql.functions import col
from pyspark.sql.types import StringType
from pyspark.sql.functions import udf
from pyspark.sql.functions import max as sparkMax
from pyspark.sql.functions import sum as sparkSum
from pyspark.sql.functions import *
from pyspark.sql.types import *





spark = SparkSession.builder.appName("Spark project").getOrCreate()





PropertyData = spark.read.csv('Property-Prices-in-Tunisia.csv', header=True)





print("The number of all properties : "+ str(PropertyData.count()))





print("The properties categories and cities of the region that contains 'Ariana' in its name")
PropertyData.filter(PropertyData.city.contains("Ariana")).select(PropertyData.category,PropertyData.city).show()





print("The houses and the villlas that the price of rent is less than 600")
PropertyData.filter((PropertyData.price < 600) & (PropertyData.type == "For rent") & (PropertyData.category == "Houses and Villas")).select(PropertyData.size,PropertyData.region,PropertyData.price).show()





print("The luxurious apartments for sale,per region, sorted by price")
PropertyData.filter((PropertyData.price > 500000) & (PropertyData.type == "For sale") & (PropertyData.category == "Apartments")).orderBy(Functions.asc('price')).select(PropertyData.region,PropertyData.price,PropertyData.size).show(truncate=False)





print("The number of properties for sale and for rent per Category and per region")
PropertyData.groupBy(PropertyData.category,PropertyData.type,PropertyData.region).count().show()





print("The number of properties, per Category, for each number of rooms")
PropertyData.groupBy(PropertyData.room_count,PropertyData.region).count().show()





print("The cumulative price for each category of properties per region")
PropertyData.groupBy(PropertyData.category,PropertyData.region).agg(Functions.sum(col("price")).alias("cumulative price")).show()





print("the Properties that have the highest price per region and per category")
PropertyData.groupBy(col("region"),col("category")).agg(sparkMax(col("price"))).show()





print("the properties of 'Ennasr' that have the highest price according to the number of rooms and bathrooms")
PropertyData.filter(col("region")=="Ennasr").groupBy(col("room_count"),col("bathroom_count")).agg(sparkMax(col("price"))).show()





print("The average of prices of apartments for sale with 2 rooms in the Region of 'Hammamet Nord'")
filteredData= PropertyData.filter((col("category")=="Apartments") & (col("region")=="Hammamet Nord") & (col("type")=="For sale") & (col("price").isNotNull()) & (col("room_count")==2))
filteredData.groupBy(col("region")).agg(avg(col("price"))).show()





print("The price of meter2 in the Region of Raoued")
PropertyData.filter((col("category")=="Houses and Villas") & (col("region")=="Raoued")).groupBy(col("region")).agg(sparkSum(col("price")),sparkSum(col("size"))).withColumn('priceM2', (Functions.col('sum(price)')/Functions.col('sum(size)'))).show()

