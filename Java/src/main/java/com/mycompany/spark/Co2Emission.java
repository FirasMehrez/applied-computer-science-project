package com.mycompany.spark;

public class Co2Emission {
	
	public String make;
	public String model;
	public String vehicle_class;
	public double engine_size;
	public int cylinders;
	public String transmission;
    public String fuel_type;
    public double fuel_con_city;
    public double fuel_con_hwy;
    public double fuel_con_comb;
    public double fuel_con_comb_mpg;
    public int co2_emission;
    
    
	public Co2Emission() {
		
	}


	public Co2Emission(String make, String model, String vehicle_class, double engine_size, int cylinders,
			String transmission, String fuel_type, double fuel_con_city, double fuel_con_hwy, double fuel_con_comb,
			double fuel_con_comb_mpg, int co2_emission) {
		super();
		this.make = make;
		this.model = model;
		this.vehicle_class = vehicle_class;
		this.engine_size = engine_size;
		this.cylinders = cylinders;
		this.transmission = transmission;
		this.fuel_type = fuel_type;
		this.fuel_con_city = fuel_con_city;
		this.fuel_con_hwy = fuel_con_hwy;
		this.fuel_con_comb = fuel_con_comb;
		this.fuel_con_comb_mpg = fuel_con_comb_mpg;
		this.co2_emission = co2_emission;
	}


	public String getMake() {
		return make;
	}


	public void setMake(String make) {
		this.make = make;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getVehicle_class() {
		return vehicle_class;
	}


	public void setVehicle_class(String vehicle_class) {
		this.vehicle_class = vehicle_class;
	}


	public double getEngine_size() {
		return engine_size;
	}


	public void setEngine_size(double engine_size) {
		this.engine_size = engine_size;
	}


	public int getCylinders() {
		return cylinders;
	}


	public void setCylinders(int cylinders) {
		this.cylinders = cylinders;
	}


	public String getTransmission() {
		return transmission;
	}


	public void setTransmission(String transmission) {
		this.transmission = transmission;
	}


	public String getFuel_type() {
		return fuel_type;
	}


	public void setFuel_type(String fuel_type) {
		this.fuel_type = fuel_type;
	}


	public double getFuel_con_city() {
		return fuel_con_city;
	}


	public void setFuel_con_city(double fuel_con_city) {
		this.fuel_con_city = fuel_con_city;
	}


	public double getFuel_con_hwy() {
		return fuel_con_hwy;
	}


	public void setFuel_con_hwy(double fuel_con_hwy) {
		this.fuel_con_hwy = fuel_con_hwy;
	}


	public double getFuel_con_comb() {
		return fuel_con_comb;
	}


	public void setFuel_con_comb(double fuel_con_comb) {
		this.fuel_con_comb = fuel_con_comb;
	}


	public double getFuel_con_comb_mpg() {
		return fuel_con_comb_mpg;
	}


	public void setFuel_con_comb_mpg(double fuel_con_comb_mpg) {
		this.fuel_con_comb_mpg = fuel_con_comb_mpg;
	}


	public int getCo2_emission() {
		return co2_emission;
	}


	public void setCo2_emission(int co2_emission) {
		this.co2_emission = co2_emission;
	}


	@Override
	public String toString() {
		return "Co2Emission [make=" + make + ", model=" + model + ", vehicle_class=" + vehicle_class + ", engine_size="
				+ engine_size + ", cylinders=" + cylinders + ", transmission=" + transmission + ", fuel_type="
				+ fuel_type + ", fuel_con_city=" + fuel_con_city + ", fuel_con_hwy=" + fuel_con_hwy + ", fuel_con_comb="
				+ fuel_con_comb + ", fuel_con_comb_mpg=" + fuel_con_comb_mpg + ", co2_emission=" + co2_emission + "]";
	}
	
	
    
    





    


    

    

}
