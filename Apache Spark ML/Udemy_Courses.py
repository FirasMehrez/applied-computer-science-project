#!/usr/bin/env python
# coding: utf-8

# In[2]:


import findspark


# In[3]:


findspark.init()


# In[4]:


import pyspark


# In[5]:


from pyspark.sql import SparkSession


# ### Create Spark Session

# In[6]:


spark = SparkSession.builder.appName('UdemyCoursesSparkML').getOrCreate()


# ### Read Data From CSV

# In[20]:


data = spark.read.csv('udemy.csv',inferSchema=True,header=True)


# ### Import Linear regression from spark ML

# In[8]:


from pyspark.ml.regression import LinearRegression


# In[21]:


data.head()


# ### Import Vector assembler 

# In[7]:


from pyspark.ml.linalg import Vectors
from pyspark.ml.feature import VectorAssembler


# In[308]:


data.show()


# ### Create vector assembler in order to put num_reviws and num_subscribers the columns in one vector "Features"

# In[309]:


assembler = VectorAssembler(inputCols = ['num_reviews','num_subscribers'],
                           outputCol='features')


# In[310]:


output = assembler.transform(data)


# In[311]:


output.select('features').show()


# In[312]:


final_data = output.select('features','num_lectures')


# In[313]:


final_data.show()


# In[314]:


output.head(1)


# ### Split the vector into two vectors one for training data the other one for test data ( 80 % , 20 % )

# In[315]:


train_data,test_data = final_data.randomSplit([0.8,0.2])


# In[316]:


train_data.describe().show()


# ### Init Linear regression and set labelCol to predict is num_lectures

# In[317]:


lr = LinearRegression(featuresCol='features',labelCol='num_lectures')


# ### Train dataa

# In[318]:


lr_model = lr.fit(train_data)


# ### Get the test results and evaluate the data

# In[319]:


test_results = lr_model.evaluate(test_data)


# ### Get the RMSE

# In[320]:


train_results.rootMeanSquaredError


# In[321]:


test_results.residuals.show()

