package com.mycompany.spark;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main1 {

	public static void main(String[] args) throws IOException {
		List<Co2Emission> co2emissions = readBooksFromCSV("CO2_Emissions_Canada.csv");
		
		/************total number of cars************/
		System.out.println("Total number of cars = " + co2emissions.stream().count());
		
		
		System.out.println("************filter cars by the engine size************");
		co2emissions.stream().filter(e -> e.getEngine_size() <1.5)
							 .collect(Collectors.toList())
							 .forEach(e -> System.out.println(e));
		
		System.out.println("************filter cars by the fuel type************");
		co2emissions.stream().filter(e -> e.getFuel_type().startsWith("Z"))
					         .limit(5).collect(Collectors.toList())
					         .forEach(e->System.out.println(e));
		
		System.out.println("******************* Cars which has the most Co2Emission by Make******************************");
		System.out.println(co2emissions.stream()
									   .filter(p -> p.getMake().equals("BMW"))
									   .max(Comparator.comparing(Co2Emission::getCo2_emission)));
			


		
		System.out.println("/************get cars model via make,vehicle class, and fuel type************");
		co2emissions.stream().filter(p-> p.getMake().equals("CHEVROLET") && p.getVehicle_class().equals("FULL-SIZE")
				&& ((p.getFuel_type().equals("Z"))||(p.getFuel_type().equals("X"))||(p.getFuel_type().equals("D"))))
					.forEach(e->System.out.println(" Model: " +e.getModel()));
		
		System.out.println("************get cars Model which their Fuel_consumption_City is under 7 per 100 KM ************");
		co2emissions.stream().filter(p->p.getFuel_con_city()<7)
							 .sorted(Comparator.comparing(Co2Emission::getFuel_con_city))
							 .forEach(e->System.out.println( "Make: " + e.getMake()+"  "
							  + " Model: " + e.getModel()+ "  " 
							  + "Fuel_consumption_City: " 
							  + e.getFuel_con_city()));

		System.out.println("************get cars Model which their Fuel_consumption_highway is under 7 per 100 KM ************");
		co2emissions.stream().filter(p->p.getFuel_con_hwy()<7)
							 .sorted(Comparator.comparing(Co2Emission::getFuel_con_hwy))
							 .forEach(e->System.out.println( "Make: " + e.getMake()+"  "
							  + " Model: " + e.getModel()+ "  " 
							  + "Fuel_consumption_highway: " 
							  + e.getFuel_con_hwy()));
		
		
		System.out.println("************get cars Model which their Engine_size is under 3 ************");
		co2emissions.stream().filter(p->p.getEngine_size()<3)
					         .sorted(Comparator.comparing(Co2Emission::getEngine_size))
					         .limit(10)
					         .forEach(e->System.out.println( "Make: " + e.getMake()+"  "
					          + " Model: " + e.getModel()+ "  " + "Engine_size: " + e.getEngine_size()));

		System.out.println("************get cars Model which their numbers of Cylinders is 4  ************");
		co2emissions.stream().filter(p->p.getCylinders()==4)
							 .sorted(Comparator.comparing(Co2Emission::getCylinders))
							 .limit(10)
							 .forEach(e->System.out.println( "Make: " + e.getMake()+"  "
							  + " Model: " + e.getModel()+ "  " 
							  + "Cylinders: " + e.getCylinders()+" "
							  +"Engine_size: " + e.getEngine_size()));
							  
		
		System.out.println("************Display Cars Co2Emission filtering by engineSize and grouping by Make************");
		System.out.println(co2emissions.stream()
								  .filter(e ->(e.getEngine_size() > 1 && e.getEngine_size()< 3) && 
								  e.getMake().equals("FIAT"))
								  .collect(Collectors.groupingBy(Co2Emission::getCo2_emission, Collectors.counting()))
								  .toString());
								  
		
	}

	private static List<Co2Emission> readBooksFromCSV(String fileName) {
		List<Co2Emission> co2emissions = new ArrayList<>();
		Path pathToFile = Paths.get(fileName);
		// create an instance of BufferedReader
		// using try with resource, Java 7 feature to close resources
		try (BufferedReader br = Files.newBufferedReader(pathToFile, StandardCharsets.UTF_8)) {
			// read the first line from the text file
			String line = br.readLine();
			// loop until all lines are read
			while (line != null) {
				// use string.split to load a string array with the values from // each line of
				// // the file, using a comma as the delimiter
				String[] attributes = line.split(",");
				Co2Emission co2emission = createco2emission(attributes);
				// System.out.println(wine.toString());
				// adding book into ArrayList
				co2emissions.add(co2emission);
				// read next line before looping // if end of file reached, line would be null
				line = br.readLine();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return co2emissions;
	}

	public static Co2Emission createco2emission(String[] metadata) {
		

        String make = metadata[0];
		String model = metadata[1];
		String vehicle_class = metadata[2];
		double engine_size = Double.parseDouble(metadata[3]);
	    int cylinders = Integer.parseInt(metadata[4]);
	    String transmission = metadata[5];
		String fuel_type = metadata[6];
		double fuel_con_city = Double.parseDouble(metadata[7]);
		double fuel_con_hwy = Double.parseDouble(metadata[8]);
		double fuel_con_comb = Double.parseDouble(metadata[9]);
		double fuel_con_comb_mpg = Double.parseDouble(metadata[10]);
		int co2_emission = Integer.parseInt(metadata[11]);

       
        return new Co2Emission(make,model,vehicle_class,engine_size,cylinders,transmission,fuel_type,fuel_con_city,fuel_con_hwy,fuel_con_comb,fuel_con_comb_mpg,co2_emission);
	
	}

}
